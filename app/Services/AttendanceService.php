<?php

namespace App\Services;

use App\Models\Attendance;
use App\Models\Leave;

class AttendanceService extends BaseService
{
    public function __construct(Attendance $attendance)
    {
        $this->model = $attendance;
    }
}
