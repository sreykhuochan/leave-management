<?php

namespace App\Services;

use App\Models\Leave;

class LeaveService extends BaseService
{
    public function __construct(Leave $leave)
    {
        $this->model = $leave;
    }
}
