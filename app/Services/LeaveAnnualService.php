<?php

namespace App\Services;

use App\Models\LeaveAnnual;

class LeaveAnnualService extends BaseService
{
    public function __construct(LeaveAnnual $leave)
    {
        $this->model = $leave;
    }
}
