<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Roles extends Model
{
    use HasFactory, SoftDeletes;

    protected $table = 'roles';

    protected $fillable = [
        'title'
    ];

    public function users()
    {
        return $this->belongsToMany(User::class,'role_users','role_id','user_id');
    }

    public function permissions()
    {
        return $this->belongsToMany(Permissions::class,'permission_roles','role_id','permission_id');
    }
}
