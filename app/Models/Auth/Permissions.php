<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Permissions extends Model
{
    use SoftDeletes, HasFactory;

    protected $table = 'permissions';

    protected $fillable = [
        'title',
    ];

    public function permissionRoles()
    {
        return $this->belongsToMany(Roles::class,'permission_roles','permission_id','role_id');
    }
}
