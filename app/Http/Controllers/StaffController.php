<?php

namespace App\Http\Controllers;

use App\Models\Department;
use App\Models\Leave;
use App\Models\Staff;
use App\Models\User;
use App\Models\UserRole;
use App\Services\StaffService;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Spatie\Permission\Models\Role;

class StaffController extends ParentController
{
    public function __construct(
        Staff $staff,
        StaffService $staffServices
    )
    {
        $this->model = $staff;
        $this->service = $staffServices;
    }
    public function countUsers()
    {
        $usersCount = User::join('user_roles', 'user_roles.id', '=', 'users.role_id')
        ->whereNotIn('users.role_id', [1, 3])->count();
        $deptCount = Department::count();
        $leaveCount = Leave::count();
        $count = DB::table('attendances')
           ->whereDate('created_at', '=', date('Y-m-d'))
           ->count();

        $response = [
            'success' => true,
            'message' => "OK",
            'User' => $usersCount,
            "Dept" => $deptCount,
            'Leave' => $leaveCount,
            "day" => $count
        ];
        return response()->json($response, 200);
    }

    public function create(Request $request): JsonResponse
    {
        $request['password'] =bcrypt($request['password']);
         return parent::create($request);
    }
    public function all(): JsonResponse
    {
        return parent::all();
    }
   public function update(Request $request, $id): JsonResponse
   {
        $request['password'] =bcrypt($request['password']);
       return parent::update($request, $id);
   }
   public function delete($id): JsonResponse
   {
       return parent::delete($id);
   }
   public function getALL(): JsonResponse
   {
        return parent::getWithRelation(['departments']);
   }
   public function ListTable()
   {
        // $data = DB::select('SELECT * FROM users ORDER BY id');
        $data = DB::table('departments')
            ->join('users', 'users.dept_id', '=', 'departments.id')
            ->orderBy('users.id')->get();
        // return $data;

        $response = [
            'success' => true,
            'message' => "OK",
            'data' => $data
        ];
        return response()->json($response, 200);
   }

   public function dataTable(Request $request, $query = null): JsonResponse
   {
        $query = DB::table('departments')
            ->join('users', 'users.dept_id', '=', 'departments.id')
            ->orderBy('users.id');
       return parent::dataTable($request, $query);
   }

   public function getStaff($id)
   {
        $staff = DB::table('users')
        ->leftJoin('departments', 'departments.id', '=', 'users.dept_id')
        ->join('user_roles', 'user_roles.id', '=', 'users.role_id')
        ->where('users.id', $id)
        ->select("*", 'users.id as user_id')->get();

        return $staff;
   }

   public function reset($id, Request $request )
    {
        // Validate the request data
        $request->validate([
            'password' => 'required|min:6',
        ]);
        // Retrieve the user from the database
        $user = User::find($id);

        // Update the user's password
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json(['message' => 'Password reset successfully.']);
    }

    public function notification($id)
    {
        $result = DB::table('users')
        ->join('attendances', 'attendances.user_id', '=', 'users.id')
        ->where(function ($query) {
            $query->where('attendances.status_id', 1)
                ->orWhere('attendances.status_id', 2);
        })
        ->where(function ($query) use ($id) {
            $query->where('attendances.approve_id', $id)
                ->orWhere('attendances.accept_id', $id);
        })
        ->orderByDesc('attendances.id')
        ->orderBy('attendances.date_request')
        ->select("*")
        ->get();

        return $result;
    }

    public function uploadImg(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);
        $user = User::find($request->input('id'));
        if (!$user) {
            return response()->json(['message' => 'User not found'], 404);
        }
        $image = $request->file('image');
        $imageName = time() . '.' . $image->extension();
        $image->storeAs('public', $imageName);

        $user->image = $imageName;
        $user->save();

        $response = [
            'path' => '/storage/' . $imageName,
            'data' => $user,
            'message' => 'Success',
        ];
        return response()->json($response, 200);
    }


}
