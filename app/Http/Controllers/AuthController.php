<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;

class AuthController extends Controller
{
    public function index(Request $request)
    {
           // first check if you are loggedin and admin user ...
        //return Auth::check();
        if (!Auth::check() && $request->path() != 'login') {
            // return redirect('/login');
            // return redirect()->route('login');
            return view('welcome');
        }

        // if (!Auth::check() && $request->path() == 'login') {

            // return view('home');
        // }
        // // you are already logged in... so check for if you are an admin user..
        $user = Auth::user();
        return $this->checkForPermission($user, $request);
    }

    public function checkForPermission($user, $request)
    {
        $permission = json_decode($user->role->permission);
        $hasPermission = false;
        if (!$permission) {
            return view('welcome');
        }

        foreach ($permission as $p) {
            if ($p->name == $request->path()) {
                if ($p->read) {
                    $hasPermission = true;
                }
            }
        }
        if ($hasPermission) {
            return view('welcome');
        }

        // return view('welcome');
        // return view('notfound');
    }

    public function register(Request $request){
        //validator
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'gender' => 'required|in:male,female',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            // 'start_date'=> 'date_format:Y-m-d',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'message' => $validator->error()
            ];
            return response()->json($response, 400);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);

        $user_role = Role::where(['name' => 'user'])->first();
        if($user_role) {
            $user->assignRole($user_role);
        }

        $success['token'] = $user->createToken('MyApp')->plainTextToken;
        $success['roles'] = $user->roles->pluck('name') ?? [];
        $success['permission'] = $user->permissions->pluck('name') ?? [];
        $success['roles_permissions'] = $user->getPermissionsViaRoles()-> pluck(['name']) ?? [];
        $success['name'] = $user->first_name;

        $response = [
            'success' => true,
            'data' => $success,
            'message' => "User register successfully!!"
        ];

        return response()->json($response, 200);
    }

    public function login(Request $request){
        if (Auth::attempt([
            'name' => $request->name,
            'password' => $request->password])) {
                // $user = Auth::user();
                $user = $request->user();
                $success['token'] = $user->createToken('MyApp')->plainTextToken;
                $success['name'] = $user->name;

                $response = [
                    'success' => true,
                    'data' => $success,
                    'message' => "User login successfully!!"
                ];
                return response()->json($response, 200);
        }else {
            $response = [
                'success' => false,
                'message' => 'Unauthorised'
            ];
            return response()->json($response);
        }
    }
    public function change_password(Request $request) {
        $validator = Validator::make($request->all(), [
            'old_password' => 'required',
            'password' => 'required|min:6'
        ]);
        if ($validator->fails()) {
            return response()->json([
                'message' => 'Validations fails',
                'errors' => $validator->errors()
            ], 422);
        }

        $user = $request->user();
        if(Hash::check($request->old_password, $user->password)) {
            $user->update([
                'password'=>Hash::make($request->password)
            ]);
            return response()->json([
                'message'=>'Password successfully updated'
            ],200);
        }else {
            return response()->json([
                'message'=>'Old password does not matched'
            ],400);
        }
    }

    public function refresh()
    {
        return response()->json([
            'user' => Auth::user(),
            // 'authorisation' => [
            //     'token' => Auth::refresh(),
            //     'type' => 'bearer',
            // ]
        ]);
    }
}
