<?php

use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ProductController;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::controller(AuthController::class)->group(function() {
    Route::post('login', 'login');
    Route::post('register', 'register');
    Route::post('refresh', 'refresh');
});

// Protected route
Route::group(['middleware' => ['auth:sanctum']], function() {
    Route::post('/logout', [AuthController::class, 'logout']);
    Route::post('/change-password', [AuthController::class, 'change_password']);

    // Route::group(['prefix' => 'v1'], function() {
    //     require __DIR__ . '/v1/staff.php';
    //     require __DIR__ . '/v1/department.php';
    //     require __DIR__ . '/v1/leave.php';
    //     require __DIR__ . '/v1/attendance.php';
    //     require __DIR__ . '/v1/leaveAnnual.php';
    //     require __DIR__ . '/v1/role.php';
    // });
});

Route::group(['prefix' => 'v1'], function() {
    require __DIR__ . '/v1/staff.php';
    require __DIR__ . '/v1/department.php';
    require __DIR__ . '/v1/leave.php';
    require __DIR__ . '/v1/attendance.php';
    require __DIR__ . '/v1/leaveAnnual.php';
    require __DIR__ . '/v1/role.php';
});


