<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('staff', function (Blueprint $table) {
            $table->id();
            // $table->integer('dept_id')->nullable();
            // $table->string('name');
            // $table->string('email')->unique();
            $table->string('attachment')->nullable();
            // $table->string('password');
            // $table->string('address')->nullable();
            // $table->date('start_date')->nullable();
            $table->timestamps();
            // $table->rememberToken();
            // $table->boolean('active')->default(true);

            // $table->foreign('dept_id')
            // ->references('id')
            // ->on('departments')
            // ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('staff');
    }
};
