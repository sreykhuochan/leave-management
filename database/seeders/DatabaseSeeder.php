<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        $this -> call(DepartmentSeeder::class);
        $this -> call(UserSeeder::class);
        $this -> call(LeaveSeeder::class);
        $this -> call(StatusSeeder::class);
        $this -> call(YearSeeder::class);
        $this -> call(RolesTableSeeder::class);
        // $this -> call(PermissionsTableSeeder::class);
        // $this -> call(PermissionRoleTableSeeder::class);
        // $this -> call(RoleUserTableSeeder::class);
    }
}
