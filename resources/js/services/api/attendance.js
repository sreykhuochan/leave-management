import Api from "./Api";

export default {
    createAtte(data) {
        return Api().post("attendance/create", data)
    },

    Apply(data) {
        return Api().post("attendance/applyLeave", data)
    },

    Remaining(id) {
        return Api().post(`attendance/remaining/${id}`)
    },

    MyLeave(id) {
        return Api().get(`attendance/my_leave/${id}`)
    },

    approveLeave(id) {
        return Api().get(`attendance/approveLeave/${id}`)
    },

    dataTable(data) {
        return Api().post("attendance/dataTable", data)
    },

    UploadImge(data) {
        return Api().post("attendance/upload", data)
    },

    pending(data) {
        return Api().post(`attendance/pending`, data)
    },

    approve(data) {
        return Api().post('attendance/approve', data)
    },

    reject(data) {
        return Api().post('attendance/reject', data)
    },

    // leave view
    leaveView(data) {
        return Api().get('attendance/leave_view', data)
    },
}
