import employees from "./employee";
import department from "./department";
import leave from "./leave";
import attendances from "./attendance";

export default {
    ...employees,
    ...department,
    ...leave,
    ...attendances,
}
