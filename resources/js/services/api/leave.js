import Api from "./Api";

export default {
    createLeave(data) {
        return Api().post("leave/create", data)
    },
    getLeave() {
        return Api().get("leave/Deptall")
    },
    deleteLeave(id) {
        return Api().delete(`leave/delete/${id}`)
    },

    updateLeave(id, data) {
        return Api().put(`leave/update/${id}`, data);
    },

    listTable() {
        return Api().get("leave/listTable")
    },

    createLeaveAnnual(data) {
        return Api().post("leave_annual/create", data)
    },

    filterLeave(id) {
        return Api().get(`leave_annual/getLeaveStaff/${id}`)
    },


}
