import Api from "./Api";

export default {
    createEmployees(data) {
        return Api().post("staff/create", data)
    },

    getEmployees() {
        return Api().get("staff/all")
    },

    deleteEmployee(id) {
        return Api().delete(`staff/delete/${id}`)
    },

    updateEmployee(id, data) {
        return Api().put(`staff/update/${id}`, data);
    },

    getAllEmployee() {
        return Api().get("staff/getAll")
    },

    listTable() {
        return Api().get("staff/listTable")
    },

    dataTable(data) {
        return Api().post("staff/dataTable", data)
    },

    getUser(id) {
        return Api().get(`staff/getStaff/${id}`)
    },

    countUser() {
        return Api().get("staff/userCount")
    },

    Notification(id) {
        return Api().get(`staff/notification/${id}`)
    }
};
