import Api from "./Api";

export default {
    createRole(data) {
        return Api().post("role/create", data)
    },

    deleteRole(id) {
        return Api().delete(`role/delete/${id}`)
    },

    updateRole(id, data) {
        return Api().put(`role/update/${id}`, data);
    },

    listTable() {
        return Api().get("role/listTable")
    },

    roleUsers() {
        return Api().get("role/roleUsers")
    },

    updateUser(id, data) {
        return Api().put(`role/updateUser/${id}`, data);
    },

    assignRole(data) {
        return Api().post("role/assignRole", data)
    },

    Users() {
        return Api().get("role/Users")
    },

    Directors() {
        return Api().get("role/Directors")
    },
}
