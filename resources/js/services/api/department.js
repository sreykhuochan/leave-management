import Api from "./Api";

export default {
    createDept(data) {
        return Api().post("department/create", data)
    },

    getDept() {
        return Api().get("department/Deptall")
    },

    deleteDept(id) {
        return Api().delete(`department/delete/${id}`)
    },

    updateDept(id, data) {
        return Api().put(`department/update/${id}`, data);
    },

    listTable() {
        return Api().get("department/listTable")
    },

    countDept() {
        return Api().get("department/DeptCount")
    }
}
