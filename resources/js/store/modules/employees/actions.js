import api from '../../../services/api'
import Swal from 'sweetalert2';
import router from '../../../router';

const actions = {
    async createEmployees({ commit }, data) {
        console.log(data);
        return await api.createEmployees(data)
            .then(({ data }) => {
                console.log("create employee", data);
                commit("CREATE_EMPLOYEES", data);
                if (data.data == null) {
                    Swal.fire({
                        title: "Oops",
                        text: "Fail to register, something went wrong!",
                        icon: "error",
                    });
                }else {
                    Swal.fire({
                        title: "Success",
                        text: "Create Successfully!",
                        icon: "success",
                    });
                    setTimeout(function() {
                        window.location.reload();
                    }, 2000);
                    router.push('/manageEmployee');
                }
            })
            .catch((err) => {
                console.log("create error", err);
                Swal.fire({
                    title: "Oops",
                    text: "Fail to register, something went wrong!",
                    icon: "error",
                });
            });
    },

    async getEmployees({ commit }) {
        return await api.getEmployees()
        .then((res) => {
            console.log("All employee", res.data);
            commit("SET_EMPLOYEES", res.data)
        })
    }
}

export default actions;
