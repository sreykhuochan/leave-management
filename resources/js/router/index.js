import { createRouter, createWebHistory } from "vue-router";
// Auht
import Login from '../pages/Authentication/Login.vue'
import Register from '../pages/Authentication/Register.vue'
import ForgotPass from '../pages/Authentication/ForgotPass.vue'
// pages
import notFound from '../Components/notFound.vue'
import Dashboard from '../pages/Dashboard.vue'
//Employee
import AddEmployee from '../pages/Employee/AddEmployee.vue'
import ManageEmployee from '../pages/Employee/ManageEmployee.vue'
import viewDetail from '../pages/Employee/viewDetail.vue'
import Profile from '../pages/Employee/Profile.vue'
//Leave
import ApplyLeave from '../pages/Leave/ApplyLeave.vue'
import ManageLeave from '../pages/Leave/ManageLeave.vue'
import LeaveView from '../pages/Leave/LeaveView.vue'
import MyLeave from '../pages/Leave/MyLeave.vue'
//Setting
import Deparment from '../pages/Setting/Department.vue'
import LeaveType from '../pages/Setting/LeaveType.vue'
import Role from '../pages/Setting/Role.vue'
import Permission from '../pages/Setting/Permission.vue'
import AdminUsers from '../pages/Setting/AdminUsers.vue'

const routes = [
    {
        path: '/',
        name: 'Login',
        component: Login,
        meta: {
            allowAnonymous: true,
            requiresAuth: false
        }
    },
    {
        path: '/register',
        name: 'Register',
        component: Register,
        meta: {
            allowAnonymous: true,
            requiresAuth: false
        }
    },
    {
        path: '/forgot-password',
        name: 'ForgotPass',
        component: ForgotPass,
        meta: {
            allowAnonymous: true,
            requiresAuth: false
        }
    },
    // pages
    {
        path: '/dashboard',
        name: 'Dashboard',
        component: Dashboard,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard' }
              ]
        }
    },
    {
        path: '/Profile/:id',
        name: 'Profile',
        component: Profile,
        meta: {
            requiresAuth: true,
        },
    },
    {
        path: '/AddEmployee',
        name: 'AddEmployee',
        component: AddEmployee,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Add Employee' }
            ]
        },
    },
    {
        path: '/manageEmployee',
        name: 'ManageEmployee',
        component: ManageEmployee,
       meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Manage Employee' }
            ]
        },
    },
    {
        path: '/manageEmployee/viewDetail/:id',
        name: 'ViewDetail',
        component: viewDetail,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'ManageEmployee', href: 'ManageEmployee' },
                { name: 'ViewDetail'}
          ]
        },
    },
    {
        path: '/applyLeave',
        name: 'ApplyLeave',
        component: ApplyLeave,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Apply Leave' }
            ]
        }
    },
    {
        path: '/manangeLeave',
        name: 'ManageLeave',
        component: ManageLeave,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Manage Leave' }
            ]
        }
    },
    {
        path: '/leaveView',
        name: 'LeaveView',
        component: LeaveView,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Leave View' }
            ]
        }
    },
    {
        path: '/department',
        name: 'Department',
        component: Deparment,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Department' }
            ]
        }
    },
    {
        path: '/leave_type',
        name: 'LeaveType',
        component: LeaveType,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Leave Type' }
            ]
        }
    },
    {
        path: '/my_leave',
        name: 'MyLeave',
        component: MyLeave,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'My Leave' }
            ]
        }
    },
    {
        path: '/position',
        name: 'Position',
        component: Role,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Position' }
            ]
        }
    },
    {
        path: '/permission',
        name: 'Permission',
        // component: Permission,
        // meta: {
        //     requiresAuth: true,
        //     breadcrumb: [
        //         { name: 'Dashboard', href: 'Dashboard' },
        //         { name: 'Permission' }
        //     ]
        // }
    },
    {
        path: '/admin_user',
        name: 'AdminUsers',
        component: AdminUsers,
        meta: {
            requiresAuth: true,
            breadcrumb: [
                { name: 'Dashboard', href: 'Dashboard' },
                { name: 'Admin Users' }
            ]
        }
    },
    //notFound
    {
        path: '/:pathMatch(.*)*',
        component: notFound,
        meta: {
            allowAnonymous: true,
            requiresAuth: false
        }
    }
]
const router = createRouter({
    history: createWebHistory(),
    routes,
})

router.beforeEach((to, from) => {
    if (to.meta.requiresAuth && !localStorage.getItem('token')){
        return { name: 'Login'}
    }

    if (to.meta.requiresAuth == false && localStorage.getItem('token')){
        return { name: 'Dashboard'}
    }
})
// Set an item in localStorage with an expiration time
function setItemWithExpiration(key, value, expiration) {
    const item = {
      value: value,
      expiration: new Date().getTime() + expiration // expiration is in milliseconds
    };
    localStorage.setItem(key, JSON.stringify(item));
    setTimeout(() => {
      localStorage.removeItem(key);
    }, expiration);
  }
// Get an item from localStorage and remove it if it has expired
function getItemWithExpiration(key) {
    const item = localStorage.getItem(key);
    if (item) {
      const { value, expiration } = JSON.parse(item);
      if (new Date().getTime() > expiration) {
        localStorage.removeItem(key);
        return null;
      }
      return value;
    }
    return null;
}
// Set an item with an expiration time of 1 minute (60 seconds)
setItemWithExpiration('key', 'value', 60 * 1000);
  const value = getItemWithExpiration('key');
  if (value) {
    console.log(value);
  } else {
    console.log('Item has expired or does not exist');
  }


export default router
